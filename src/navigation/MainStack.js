import React from 'react';
import EscanearCodigo from '../views/EscanearCodigo';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

const Stack = createNativeStackNavigator();

const MainStack = () => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Escanear" component={EscanearCodigo} />
    </Stack.Navigator>
  );
};

export default MainStack;
