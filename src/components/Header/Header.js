import React from 'react';
import {View, Image} from 'react-native';
import styles from '../../assets/scanStyle';

const Header = () => {
  return (
    <View style={styles.header}>
      <Image style={styles.tinyLogo} source={require('../../assets/logoTG.png')} />
    </View>
  );
};

export default Header;
